from src.inference.exporter import ModelExporter

INPUT_MODEL_PATH = "./models_train/X_101_32x8d_FPN_3x_batch30/"
OUTPUT_MODEL_PATH = "./models_prod/"
ANN_PATH = "./data/images/3.0/full_annotations.json"
IMAGES_PATH = "./data/images/3.0/images/"
TEST_ANNS = "./data/images/3.0/test_annotations.json"

model_exporter = ModelExporter(ANN_PATH, IMAGES_PATH, TEST_ANNS)
model_exporter.export_model(INPUT_MODEL_PATH, OUTPUT_MODEL_PATH)
