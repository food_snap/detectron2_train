from typing import Any

import detectron2.data.transforms as T
from detectron2.data import DatasetMapper, build_detection_train_loader
from detectron2.engine import DefaultTrainer
from detectron2.evaluation import COCOEvaluator, DatasetEvaluators


class CustomTrainer(DefaultTrainer):
    """Custom trainer with Augmentations and evaluation"""

    @classmethod
    def build_train_loader(cls, cfg) -> Any | None:
        """Создаёт загрузчик"""

        def get_augmentations() -> list:
            """Создаёт список аугментаций"""
            min_iou_crop = T.MinIoURandomCrop(
                min_ious=[0.5, 0.6, 0.7],
                min_crop_size=0.5,
                mode_trials=1000,
                crop_trials=50,
            )

            random_iou_crop = T.RandomApply(min_iou_crop)
            random_light = T.RandomApply(T.RandomLighting(0.65))

            augmentations = [
                T.RandomFlip(prob=0.5, horizontal=False, vertical=True),
                T.RandomFlip(prob=0.5, horizontal=True, vertical=False),
                T.RandomRotation([0, 180]),
                T.RandomBrightness(0.4, 1.6),
                T.RandomSaturation(0.4, 1.6),
                T.RandomContrast(0.4, 1.6),
                T.ResizeShortestEdge(
                    short_edge_length=[100, 5000], max_size=480, sample_style="range"
                ),
                random_light,
                random_iou_crop,
            ]
            """
            Неиспользованные аугментации:
                "FixedSizeCrop"
                "RandomCrop",
                "RandomCrop_CategoryAreaConstraint",
                "RandomExtent",
                "ResizeScale",
                "Resize",
            """
            return augmentations

        def create_mapper(config, augmentations) -> DatasetMapper:
            """
            Создаёт обработчик датасета, применяющий аугментации к изображениям
            """
            mapper = DatasetMapper(config, is_train=True, augmentations=augmentations)
            return mapper

        augmentations = get_augmentations()
        augs_mapper = create_mapper(cfg, augmentations)

        return build_detection_train_loader(cfg, mapper=augs_mapper)

    @classmethod
    def build_evaluator(
        cls, cfg, dataset_name, output_folder=None
    ) -> DatasetEvaluators:
        coco_evaluator = COCOEvaluator(dataset_name, output_dir=output_folder)

        evaluator_list = [coco_evaluator]

        return DatasetEvaluators(evaluator_list)
