from detectron2.config import CfgNode


class LR_SCHEDULER:
    """Set LR decay during education"""

    def __init__(self, iter_length: int, period: int) -> None:
        """Calculate schedule list for cfg

        Args:
            iter_lenght: training iterations amount
            period: period to decay LR
        """
        self.steps_list = [s * period for s in range(1, int(iter_length / period))]
        self.iter_length = iter_length

    def set_cfg_lr(self, cfg: CfgNode, base_lr=0.02, gamma=0.9, warm=10000) -> CfgNode:
        """Sets LR in cfg

        Args:
            cfg: train config
            base_lr: start lr. Defaults to 0.02.
            gamma: lr multiplier. Defaults to 0.9.
            warm: offset before to start decay lr. Defaults to 10000.

        Returns:
            _description_
        """
        cfg.SOLVER.BASE_LR = base_lr
        cfg.SOLVER.GAMMA = gamma
        cfg.SOLVER.WARMUP_ITERS = warm
        cfg.SOLVER.STEPS = self.steps_list
        cfg.SOLVER.MAX_ITER = self.iter_length

        # Варианты: WarmupMultiStepLR, WarmupCosineLR.
        # Смотрим detectron2/solver/build.py для выбора
        cfg.SOLVER.LR_SCHEDULER_NAME = "WarmupMultiStepLR"
        return cfg
