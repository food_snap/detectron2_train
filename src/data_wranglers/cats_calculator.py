import json


def get_cats_amount(ann_file: str) -> int:
    """Counts number of categories in coco annotations file

    Args:
        ann_file: annotations filename

    Returns:
        number of categories
    """
    with open(ann_file) as f:
        images_data = json.load(f)
    return len(images_data["categories"])
