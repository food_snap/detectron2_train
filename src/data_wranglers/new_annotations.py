import json
import os

import cv2
import tqdm

RAW_PATH = "./data/raw/AIcrowd_FR/2.0/"
TRAIN_ANNOTATIONS_PATH = RAW_PATH + "public_training_set_release_2.0/annotations.json"
TRAIN_IMAGE_DIRECTIORY = RAW_PATH + "public_training_set_release_2.0/images/"

VAL_ANNOTATIONS_PATH = RAW_PATH + "public_validation_set_2.0/annotations.json"
VAL_IMAGE_DIRECTIORY = RAW_PATH + "public_validation_set_2.0/images/"

INTERIM_PATH = "./data/interim/"


def extract_annotations(train_path: str, val_path: str) -> tuple[dict, dict]:
    """Read json files and extract annotations dicts"""
    with open(train_path) as f:
        train_annotations = json.load(f)

    with open(val_path) as f:
        val_annotations = json.load(f)

    return train_annotations, val_annotations


def make_new_directories(path: str) -> None:
    os.makedirs(path + "train/", exist_ok=True)
    os.makedirs(path + "val/", exist_ok=True)


def fix_data(annotations: dict, directiory: str, VERBOSE=True) -> dict:
    """Function for taking a annotation & directiory of images
    and returning new annoation json with fixed image size info"""
    print(f"fixing {directiory}")
    for n, i in enumerate(tqdm((annotations["images"]))):
        img = cv2.imread(directiory + i["file_name"])

        if img.shape[0] != i["height"]:
            annotations["images"][n]["height"] = img.shape[0]
            if VERBOSE:
                print(i["file_name"])
                print(annotations["images"][n], img.shape)

        if img.shape[1] != i["width"]:
            annotations["images"][n]["width"] = img.shape[1]
            if VERBOSE:
                print(i["file_name"])
                print(annotations["images"][n], img.shape)

    return annotations


# Extracring annotations
train_annotations_data, val_annotations_data = extract_annotations(
    TRAIN_ANNOTATIONS_PATH, VAL_ANNOTATIONS_PATH
)

# fix annotations for training dataset
train_annotations_data = fix_data(train_annotations_data, TRAIN_IMAGE_DIRECTIORY)

make_new_directories(INTERIM_PATH)  # directories for new annotations

with open(INTERIM_PATH + "train/new_ann.json", "w") as f:
    json.dump(train_annotations_data, f)

# similar processing for validation data
val_annotations_data = fix_data(val_annotations_data, VAL_IMAGE_DIRECTIORY)

with open(INTERIM_PATH + "val/new_ann.json", "w") as f:
    json.dump(val_annotations_data, f)
