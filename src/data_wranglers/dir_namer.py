def make_model_dirname(model_arch: str, batchsize: int, postfix: str = "") -> str:
    """Создаёт имя директории для модели

    Args:
        model_arch (str): ссылка на архитерктуру
        batchsize (int): _размер батча
        postfix (str, optional): _произвольная подпись. По умолчанию ''.

    Returns:
        str: название директории модели
    """
    dirname = model_arch.split("/")[-1].split(".")[0].replace("mask_rcnn_", "")
    dirname = f"{dirname}_batch{str(batchsize)}{postfix}"
    return dirname
