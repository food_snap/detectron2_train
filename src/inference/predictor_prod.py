import cv2
import torch


class CategoryPredictor:
    """
    A class for predicting categories of images using a pre-trained model.
    """

    def __init__(self, model_path) -> None:
        """
        Initialize the CategoryPredictor object.

        Parameters:
        model_path (str): The path to the pre-trained model file.

        Returns:
        None
        """
        self.model = torch.jit.load(model_path)

    def preprocess_image(self, image_file) -> torch.Tensor:
        """
        Preprocess an image for prediction.

        Parameters:
        image_file (str): The path to the image file.

        Returns:
        torch.Tensor: The preprocessed image as a tensor.
        """
        im = cv2.imread(image_file)
        return torch.from_numpy(im.transpose(2, 0, 1))

    def extract_predictions(self, prediction) -> dict:
        """
        Extract the predictions from the model output.

        Parameters:
        prediction (torch.Tensor): The output of the model.

        Returns:
        dict: A dictionary mapping classes to their corresponding scores.
        """
        classes = prediction[1].tolist()
        scores = prediction[3].tolist()
        percents = [round(s, 2) for s in scores]
        return dict(zip(classes, percents))

    def get_prediction(self, image_file: str) -> dict:
        """
        Get the prediction for an image.

        Parameters:
        image_file (str): The path to the image file.

        Returns:
        dict: A dictionary mapping classes to their corresponding scores.
        """
        image = self.preprocess_image(image_file)
        prediction = self.model(image)
        return self.extract_predictions(prediction)
