import cv2
import detectron2.data.transforms as T
from cv2_plt_imshow import cv2_plt_imshow
from detectron2.data import MetadataCatalog
from detectron2.data.datasets import load_coco_json, register_coco_instances
from detectron2.engine import DefaultPredictor
from detectron2.utils.visualizer import Visualizer

from src.inference.config_loader import load_model_config


class Predictor:
    """
    Class that creates a predictor and methods to get it results,
    such as predicted classes and images with predictions
    """

    def __init__(
        self,
        model_path: str,
        threshold: float = 0.1,
        ANN_PATH: str = None,
        IMG_PATH: str = None,
    ) -> None:
        """
        Initialise class object and create a predictor with
        given parameters

        Args:
            model_path: path to model weights and config file
            threshold: threshold for the model. Defaults to 0.5.
        """
        self.cfg = load_model_config(model_path, threshold)
        self.predictor = DefaultPredictor(self.cfg)
        self.ann_path = ANN_PATH
        self.img_path = IMG_PATH
        self.metadata = self.load_metadata()

    def predict_classes(self, image_path: str) -> dict:
        """
        Takes an image and returns dict with predicted classes and their scores

        Args:
            image_path: image to predict classes on it

        Returns:
            dict with classes and their scores
        """
        im = cv2.imread(image_path)
        image_transformed = self.transform_image(im)

        outputs = self.predictor(image_transformed)
        classes = outputs["instances"].pred_classes.tolist()
        scores = outputs["instances"].scores.tolist()
        percents = [round(s, 2) for s in scores]

        return dict(zip(classes, percents))

    def plot_prediction(self, image_path: str) -> None:
        """
        Takes an image and draws predictions on it

        Args:
            img_file: image to predict classes on it
        """
        im = cv2.imread(image_path)
        image_transformed = self.transform_image(im)

        outputs = self.predictor(image_transformed)
        v = Visualizer(image_transformed[:, :, ::-1], self.metadata, scale=0.7)

        out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
        cv2_plt_imshow(out.get_image()[:, :, ::-1])

    def transform_image(self, image):
        """Transform image"""
        aug = T.ResizeShortestEdge(
            short_edge_length=[100, 5000], max_size=480, sample_style="range"
        )
        transform = aug.get_transform(image)
        image_transformed = transform.apply_image(image)

        return image_transformed

    def load_metadata(self):
        """Loads dataset metadata if its paths are passed

        Returns:
            metadata or None
        """
        if self.ann_path is None or self.img_path is None:
            return None

        register_coco_instances("train_dataset", {}, self.ann_path, self.img_path)

        load_coco_json(self.ann_path, self.img_path, "train_dataset")

        metadata = MetadataCatalog.get("train_dataset")
        return metadata
