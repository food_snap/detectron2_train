from detectron2.config import CfgNode, get_cfg


def load_model_config(model_path: str, threshold: float) -> CfgNode:
    """
    Define detectron2 config

    Args:
        model_path: path to model weights and config file
        threshold: threshold for the model

    Returns:
        detectron2 config
    """
    cfg = get_cfg()

    # Set model url from zoo: ToDo: change it to our model
    cfg.merge_from_file(f"{model_path}/config.yaml")

    # Loading pretrained weights. ToDo: change it to the weight after our training
    cfg.MODEL.WEIGHTS = f"{model_path}/model_final.pth"

    # Set threshold for this model
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = threshold
    return cfg
