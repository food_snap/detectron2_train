from collections import OrderedDict

from detectron2.data import DatasetCatalog, build_detection_test_loader
from detectron2.data.dataset_mapper import DatasetMapper
from detectron2.data.datasets import register_coco_instances
from detectron2.engine import DefaultPredictor
from detectron2.evaluation import COCOEvaluator, inference_on_dataset

from src.inference.config_loader import load_model_config


class Evaluator:
    def __init__(
        self,
        model_path: str,
        images_path: str,
        annotations_file: str,
        threshold: float = 0.2,
    ) -> None:
        """
        Used to evaluate trained models on a given dataset.

        Args:

            threshold: threshold for the model. Defaults to 0.5.
        """
        self.cfg = load_model_config(model_path, threshold)
        self.predictor = DefaultPredictor(self.cfg)
        self.register_dataset(images_path, annotations_file)
        self.evaluator = COCOEvaluator(
            dataset_name="validation_dataset",
            distributed=False,
            output_dir=model_path + "/eval/",
        )
        dataset_mapper = DatasetMapper(self.cfg, is_train=False)
        self.val_loader = build_detection_test_loader(
            self.cfg, "validation_dataset", mapper=dataset_mapper
        )

    def register_dataset(self, images_path: str, annotations_file: str) -> None:
        if "validation_dataset" not in DatasetCatalog.list():
            register_coco_instances(
                "validation_dataset", {}, annotations_file, images_path
            )

    def evaluate_model(self) -> OrderedDict | dict:
        return inference_on_dataset(
            self.predictor.model, self.val_loader, self.evaluator
        )
