from typing import Any

import torch
from detectron2.config import CfgNode
from detectron2.data import build_detection_test_loader
from detectron2.data.datasets import register_coco_instances
from detectron2.export import TracingAdapter

from src.inference.predictor import Predictor

print(torch.__version__)


class ModelExporter:
    """
    A class for exporting a model.
    """

    def __init__(self, ann_path: str, images_path: str, test_anns: str) -> None:
        """
        Initialize the ModelExporter object.

        Parameters:
        ann_path (str): The path to the annotations file.
        images_path (str): The path to the images directory.
        test_anns (str): The path to the test annotations file.

        Returns:
        None
        """
        self.ann_path = ann_path
        self.images_path = images_path
        self.test_anns = test_anns

    def get_model_inputs(self, test_iterator) -> list[dict[str, Any]]:
        """
        Get the model inputs from the test iterator.

        Parameters:
        test_iterator (iterator): The test iterator.

        Returns:
        list[dict[str, Any]]: The model inputs.
        """
        inputs = next(test_iterator)
        inputs = [{"image": input["image"] for input in inputs}]
        return inputs

    def load_model(self, model_path) -> tuple[Any, CfgNode]:
        """
        Load the model from the given path.

        Parameters:
        model_path (str): The path to the model file.

        Returns:
        tuple[Any, CfgNode]: The loaded model and its configuration.
        """
        custom_predictor = Predictor(
            model_path, ANN_PATH=self.ann_path, IMG_PATH=self.images_path
        )
        return custom_predictor.predictor.model, custom_predictor.cfg

    def create_tl_iterator(self, cfg):
        """
        Create a test loader iterator.

        Parameters:
        cfg (CfgNode): The configuration of the model.

        Returns:
        iterator: The test loader iterator.
        """
        tld = build_detection_test_loader(cfg, "test_dataset")
        return iter(tld)

    def export_model(
        self,
        input_path: str,
        output_path: str,
    ) -> None:
        """
        Export the model to a traced model.

        Parameters:
        input_path (str): The path to the input model file.
        output_path (str): The output path to save the serialised model.
        Returns:
        None
        """
        model, cfg = self.load_model(input_path)

        register_coco_instances(
            name="test_dataset",
            metadata={},
            json_file=self.test_anns,
            image_root=self.images_path,
        )

        test_iterator = self.create_tl_iterator(cfg)

        inputs = self.get_model_inputs(test_iterator)

        wrapper = TracingAdapter(model, inputs=inputs)
        wrapper.eval()
        traced_script_module = torch.jit.trace(
            func=wrapper, example_inputs=wrapper.flattened_inputs
        )
        output_model_name = output_path + f"{input_path.split('/')[-2]}.pt"
        traced_script_module.save(output_model_name)
