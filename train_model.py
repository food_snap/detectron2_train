# Базовые библиотеки
import os

import torch
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.data.datasets import register_coco_instances  # , load_coco_json

# detectron2
from detectron2.utils.logger import setup_logger

from src.data_wranglers.cats_calculator import get_cats_amount
from src.data_wranglers.dir_namer import make_model_dirname

# Внутренние модули
from src.training.augs_trainer import TrainerWithAugmentations
from src.training.lr_scheduler import LR_SCHEDULER

# Смотрим версии библиотек
TORCH_VERSION = torch.__version__
CUDA_VERSION = torch.version.cuda
print("torch: ", TORCH_VERSION, "; cuda: ", CUDA_VERSION)

setup_logger()

# Определяем пути
TRAIN_IMAGE_DIR = "./data/images/3.0/images/"
VAL_IMAGE_DIR = "./data/images/3.0/images/"
TRAIN_ANNOTATIONS = "./data/images/3.0/full_annotations.json"
VAL_ANNOTATIONS = "./data/images/3.0/val_annotations.json"

MODEL_PATH = "./models_train/"


register_coco_instances("training_dataset", {}, TRAIN_ANNOTATIONS, TRAIN_IMAGE_DIR)

# Later use for classes comparison with ids
# load_coco_json(TRAIN_ANNOTATIONS, TRAIN_IMAGE_DIR, "training_dataset")

register_coco_instances("validation_dataset", {}, VAL_ANNOTATIONS, VAL_IMAGE_DIR)

number_of_categories = get_cats_amount(TRAIN_ANNOTATIONS)

# Выбираем предобученную модель из model_zoo
MODEL_ARCH = "COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml"

cfg = get_cfg()
cfg.merge_from_file(model_zoo.get_config_file(MODEL_ARCH))

cfg.DATASETS.TRAIN = ("training_dataset",)
cfg.DATASETS.TEST = ()
cfg.DATALOADER.NUM_WORKERS = 6
# Загружаем веса
cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(MODEL_ARCH)

cfg.SOLVER.IMS_PER_BATCH = 3

lr_scheduler = LR_SCHEDULER(iter_length=100000, period=5000)

cfg = lr_scheduler.set_cfg_lr(cfg)
cfg.SOLVER.CHECKPOINT_PERIOD = 1000  # сохраняем веса через каждые ... шагов

cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 480
cfg.MODEL.ROI_HEADS.NUM_CLASSES = number_of_categories  # количество классов в датасете

# Определение директории для сохранения истории обучения и весов модели
model_dir = make_model_dirname(MODEL_ARCH, cfg.SOLVER.IMS_PER_BATCH)
cfg.OUTPUT_DIR = f"{MODEL_PATH}{model_dir}_test"
os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)

# Поставить True если продолжаем обучение с контрольной точки
RESUME = False
trainer = TrainerWithAugmentations(cfg)
trainer.resume_or_load(resume=RESUME)

with open(f"{cfg.OUTPUT_DIR }/config.yaml", "w") as f:
    f.write(trainer.cfg.dump())  # save config to file

trainer.train()
